﻿class Cube
{
    private readonly string[,,] cube;
    public int Count { get; set; } = 0;

    public Cube() { }
    public Cube(string[,,] cube, int count)
    {
        this.cube = cube;
        this.Count = count;
    }

    public void Setxyz(int i, out int x, out int y, out int z)
    {
        i = 0;
        x = 0;
        y = 0;
        z = 0;
    }

    public void Write(string message) { }

    public void Read() { }

    public string[,] GetSlice(Axes axe, int deep) { return null; }

    public void SetSlice(Axes axe, int deep, string[,] slice) { }

    public string[,] Rotate(string[,] slice) { return null; }

    public string[,] Rotate(string[,] slice, int count) { return null; }

    public string[,] Rotate(Axes axe, int deep, int count) { return null; }
}